export default function Footer() {

  return (
    <footer className="footer">
      <p>Author: eMythMaker
        <div className="footer__left">
          <a href="https://www.emythmakers.com/">eMythMakers.com</a>
        </div>
      </p>
    </footer>
  )

}
