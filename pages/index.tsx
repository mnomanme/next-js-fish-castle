import Head from "next/head"
import Contact from "../components/Contact"
import Footer from "../components/Footer"
import Header from "../components/Header"
import { IProduct } from "../components/Product"
import ProductList from "../components/ProductList"
import "../styles.scss"


interface IIndexProps {
  products: IProduct[]
}

const Index = (props: IIndexProps) => {
  return (
    <div className="app">
      <Head>
        {/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdn.snipcart.com/scripts/2.0/snipcart.js" data-api-key="MzMxN2Y0ODMtOWNhMy00YzUzLWFiNTYtZjMwZTRkZDcxYzM4" id="snipcart"></script>
        <link href="https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css" rel="stylesheet" type="text/css" /> */}
        <link rel="shortcut icon" href="/static/favicon.ico" />

         {/* my api test mode  */}

         {/* <link rel="stylesheet" href="https://cdn.snipcart.com/themes/v3.0.26/default/snipcart.css" />
        <script async src="https://cdn.snipcart.com/themes/v3.0.26/default/snipcart.js"></script>
          <div id="snipcart" data-api-key="MTg3NzM0MDQtNjY3Ny00MTc3LTg1YTQtZmZiYzlmZTY2MTc5NjM3NDI1NzI1OTMzMjY5MzAw" hidden>
          </div> */}

          {/* my api live mode */}

          <link rel="stylesheet" href="https://cdn.snipcart.com/themes/v3.0.26/default/snipcart.css" />
          <script async src="https://cdn.snipcart.com/themes/v3.0.26/default/snipcart.js"></script>
          <div id="snipcart" data-api-key="MTg3NzM0MDQtNjY3Ny00MTc3LTg1YTQtZmZiYzlmZTY2MTc5NjM3NDI1NzI1OTMzMjY5MzAw" hidden>
          </div>

      </Head>
      <Header />
      <main className="main">
        <img src="/static/aquarium.svg" alt="a" className="background-image" />
        <div className="promotional-message">
          <h3>Burger World</h3>
          <h2>Burger Blast</h2>
          <p>An <strong>exclusive collection of chessy</strong> available for everyone.</p>
        </div>
        <ProductList products={props.products} />
        <Contact />
      </main>
      <Footer />
    </div>
  )
}

Index.getInitialProps = async () => {
  return {
    products: [
      {id: "nextjs_halfmoon", name: "Burger Bros", price: 25.00, image: "../static/01.jpg", description: "The Halfmoon betta is arguably one of the prettiest betta species. It is recognized by its large tail that can flare up to 180 degrees."} as IProduct,
      {id: "nextjs_dragonscale", name: "Burger Time", price: 35, image: "../static/02.jpg",description: "The dragon scale betta is a rarer and higher maintenance fish. It is named by its thick white scales covering his sides that looks like dragon scale armor."} as IProduct,
      {id: "nextjs_crowntail", name: "Burger Bistro", price: 17.50, image: "../static/03.jpg", description: "The crowntail is pretty common, but interesting none the less. It's recognized by the shape of its tail that has an appearance of a comb."} as IProduct,
      {id: "nextjs_veiltail", name: "The Burgery", price: 15.00, image: "../static/04.jpg", description: "By far the most common betta fish. You can recognize it by its long tail aiming downwards."} as IProduct,
    ]
  }
}

export default Index
